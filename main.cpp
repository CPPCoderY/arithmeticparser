#include <QCoreApplication>
#include <iostream>
#include <string>
#include <QVector>
#include <QString>

using namespace std;

struct ExprPart{
    bool isOperator;
    QString sym;
};

void getNums(QVector<ExprPart*> expr, float& a, float& b, int pos){
    a = expr[pos - 1]->sym.toFloat();
    b = expr[pos + 1]->sym.toFloat();
}

//void removeParts(QVector<ExprPart*> &expr, int i);

QVector<ExprPart*> calculateWithLevel(int level, QVector<ExprPart*> expr){
    for(int i = 0; i < expr.size(); i++){
        //        cout << i << " " << expr.size() << endl;
        if(expr[i]->isOperator){
            switch(level){
            case 3:
                if(expr[i]->sym == '^'){
                    float a, b;
                    getNums(expr, a, b, i);
                    expr[i - 1]->sym = QString::number(pow(a, b));
                    expr.remove(i, 2);
                    i--;
                }
                break;
            case 2:
                if(expr[i]->sym == '*'){
                    float a, b;
                    getNums(expr, a, b, i);
                    expr[i - 1]->sym = QString::number(a * b);
                    expr.remove(i, 2);
                    i--;
                } else if(expr[i]->sym == '/'){
                    float a, b;
                    getNums(expr, a, b, i);
                    expr[i - 1]->sym = QString::number(a / b);
                    expr.remove(i, 2);
                    i--;
                }
                break;
            case 1:
                if(expr[i]->sym == '+'){
                    float a, b;
                    getNums(expr, a, b, i);
                    expr[i - 1]->sym = QString::number(a + b);
                    expr.remove(i, 2);
                    i--;
                } else if(expr[i]->sym == '-'){
                    float a, b;
                    getNums(expr, a, b, i);
                    expr[i - 1]->sym = QString::number(a - b);
                    expr.remove(i, 2);
                    i--;
                }
                break;
            }
        }
    }
    return expr;
}

float calculateExpression(QVector<ExprPart*> expr){
    for(int i = 3; i  != 0; i--){
        expr = calculateWithLevel(i, expr);
    }
    return expr[0]->sym.toFloat();
}

QVector<ExprPart*> parse(QString expr){
    ExprPart* part = new ExprPart;
    QVector<ExprPart*> expression;
    for(int i = 0; i < expr.size(); i++){
        if(expr[i].isNumber() || expr[i] == '.'){
            part->sym.append(expr[i]);
        } else if (expr[i] == '*' || expr[i] == '/' || expr[i] == '+' || expr[i] == '-' || expr[i] == '^'){
            part->isOperator = false;
            expression.append(part);
            part = new ExprPart;
            part->isOperator = true;
            part->sym = expr[i];
            expression.append(part);
            part = new ExprPart;
            part->isOperator = false;
        } else if (expr[i] == '('){
            int a = i;
            while(expr[i] != ')'){
                i++;
            }
            QString newExpression = expr.mid(a + 1, i - a - 1);
            expr = expr.right(expr.size() - (i +1));
            cout << newExpression.toStdString() << endl;
            part->isOperator = false;
            part->sym = QString::number(calculateExpression(parse(newExpression)));
            i = -1;
        }
    }
    expression.append(part);
    return expression;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString expr;
    string temp;
    while(true){
        cin >> temp;
        expr = QString::fromStdString(temp);
        QVector<ExprPart*> expression = parse(expr);
        float result = calculateExpression(expression);
        cout << result;
    }
    return a.exec();
}
